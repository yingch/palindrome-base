package sheridan;


/**
 * Author: Ying Chen 991549628
 */
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		assertTrue("Input is not palomdrome", Palindrome.isPalindrome("YnggnY"));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse("Input is not palomdrome", Palindrome.isPalindrome("YingChen"));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue("Input is not palomdrome", Palindrome.isPalindrome("Y"));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("Input is not palomdrome", Palindrome.isPalindrome("Race a car"));
	}	
	
}
